/*!******************************************************************************
 * @file    gtap200_api.h
 * @brief   gtap200 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GTAP200_API_H__
#define __GTAP200_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int gtapxxx_init( unsigned int param );
void gtapxxx_ctrl( int f_ena );
unsigned int gtapxxx_rcv( unsigned int tick );
int gtapxxx_conv( frizz_fp* press_data );

int gtapxxx_setparam( void *ptr );
unsigned int gtapxxx_get_ver( void );
unsigned int gtapxxx_get_name( void );

int gtapxxx_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif
