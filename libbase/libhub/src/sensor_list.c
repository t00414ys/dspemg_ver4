/*!******************************************************************************
 * @file  sensor_if.c
 * @brief sensor interface for sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include "frizz_mem.h"
#include "sensor_if.h"
#include "sensor_list.h"
#include "base_driver.h"
#include "frizz_util.h"


//#define		USE_ALIGMENT_CHECK


struct node_t;

struct child_node_t {
	struct node_t			*p_node;
	struct child_node_t		*p_next;
};

struct node_t {
	sensor_info_t			info;
	struct node_t			*p_next;
	struct child_node_t		*p_child;
};

typedef struct {
	struct node_t			*p_link;
	sensor_list_linear_t	line;
	unsigned int			status;
} sensor_list_t;

// child_node util
static void child_node_destroy( struct child_node_t* o )
{
	if( o != 0 ) {
		frizz_free( o );
	}
}

static struct child_node_t* child_node_create( struct node_t *p_node )
{
	struct child_node_t *o = ( struct child_node_t* )frizz_malloc( sizeof( struct child_node_t ) );
	if( o != 0 ) {
		memset( o , 0 , sizeof( struct child_node_t ) );
		o->p_node = p_node;
		o->p_next = 0;
	}
	return o;
}

// node util
static void node_destroy( struct node_t *o )
{
	int	i;
	if( o != 0 ) {
		struct child_node_t *p_child = o->p_child;
		if( o->info.a_child_info != 0 ) {
			frizz_free( o->info.a_child_info );
		}
		for( i = 0; i < SENSOR_AREA_NUM; i++ ) {
			if( o->info.latest[i].data != 0 ) {
				frizz_free( o->info.latest[i].data );
			}
		}

		while( p_child != 0 ) {
			struct child_node_t *swp = p_child->p_next;
			child_node_destroy( p_child );
			p_child = swp;
		}
		frizz_free( o );
	}
}

static struct node_t* node_create( sensor_if_t *p_if )
{
	if( p_if != 0 ) {
		struct node_t *o = ( struct node_t* )frizz_malloc( sizeof( struct node_t ) );
		if( o != 0 ) {
			void *data;
			int tick, size, i;
			unsigned int data_ts;
			memset( o , 0 , sizeof( struct node_t ) );
			// get sensor parameter
			size = p_if->get.data( &data, &data_ts );
			tick = p_if->get.interval();
			// set parameter
			o->info.p_if = p_if;
			o->info.a_child_info = 0;
			o->info.child_num = 0;
			o->info.wake_ts = 0;
			o->p_next = 0;
			o->p_child = 0;
			for( i = 0; i < SENSOR_AREA_NUM; i++ ) {
				// status
				o->info.status[i] = 0;
				o->info.out_ts[i] = 0;
				o->info.tick[i] = tick;
				o->info.latest[i].ts = 0;
				o->info.latest[i].size = size;
				if( size <= 0 ) {
					o->info.latest[i].data = NULL;
				} else {
					o->info.latest[i].data = ( void* )frizz_malloc( sizeof( int ) * size );
				}
				if( ( 0 < size ) && ( o->info.latest[i].data == NULL ) ) {
					frizz_free( o );
					return 0;
				}
				if( size > 0 ) {
					memset( o->info.latest[i].data , 0 ,  sizeof( int ) * size );
				}
			}
		}
		return o;
	} else {
		return 0;
	}
}

static int node_add_child( struct node_t *p_node, struct node_t *p_child )
{
	struct child_node_t *p_child_node;
	if( p_node == 0 || p_child == 0 ) {
		return -1;
	}

	p_child_node = child_node_create( p_child );
	if( p_child_node == 0 ) {
		return -3;
	}

	p_child_node->p_next = p_node->p_child;
	p_node->p_child = p_child_node;
	return 0;
}

static void connect_dependency( struct node_t *top )
{
	for( ; top != 0; top = top->p_next ) {
		unsigned int top_par_num;
		unsigned char *top_par_ls, top_id;
		struct node_t *p_node;

		top_id = top->info.p_if->get.id();
		top_par_num = top->info.p_if->get.parent_list( &top_par_ls );
		for( p_node = top->p_next; p_node != 0; p_node = p_node->p_next ) {
			unsigned int i, node_par_num;
			unsigned char *node_par_ls, node_id;

			node_id = p_node->info.p_if->get.id();
			node_par_num = p_node->info.p_if->get.parent_list( &node_par_ls );
			// by id of top
			for( i = 0; i < node_par_num; i++ ) {
				if( node_par_ls[i] == top_id ) {
					node_add_child( top, p_node );
					break;
				}
			}
			// by id of p_node
			for( i = 0; i < top_par_num; i++ ) {
				if( top_par_ls[i] == node_id ) {
					node_add_child( p_node, top );
					break;
				}
			}
		}
	}
}

static void sub_linearize( sensor_list_t *o, struct node_t *p_node )
{
	if( o != 0 && p_node != 0 ) {
		struct child_node_t *p_child;
		for( p_child = p_node->p_child; p_child != 0; p_child = p_child->p_next ) {
			if( ( p_child->p_node->info.status[SENSOR_AREA_0] & 0x80000000 ) == 0 ) {
				sub_linearize( o, p_child->p_node );
			}
			p_node->info.a_child_info[p_node->info.child_num].p_if = p_child->p_node->info.p_if;
			p_node->info.a_child_info[p_node->info.child_num].attr = 0;
			p_node->info.a_child_info[p_node->info.child_num].tick = p_node->info.p_if->get.interval();
			p_node->info.child_num++;
		}
		if( o->line.num != 0 ) {
			o->line.num--;
			o->line.pa_sensor[o->line.num] = &( p_node->info );
		}
		p_node->info.status[SENSOR_AREA_0] |= 0x80000000;
		p_node->info.status[SENSOR_AREA_1] |= 0x80000000;
	}
}

static int sensor_list_linearize( sensor_list_t *o )
{
	struct node_t *p_node;
	unsigned int node_num = 0;

	if( o == 0 ) {
		// parameter error
		return -1;
	}

	// count
	for( p_node = o->p_link; p_node != 0; p_node = p_node->p_next ) {
		struct child_node_t *p_child;
		unsigned int child_num = 0;
		for( p_child = p_node->p_child; p_child != 0; p_child = p_child->p_next ) {
			child_num++;
		}
		p_node->info.a_child_info =
			( sensor_child_info_t* )frizz_malloc( sizeof( sensor_child_info_t ) * child_num );
		if( p_node->info.a_child_info == 0 ) {
			// malloc error
			return -3;
		}
		memset( p_node->info.a_child_info, 0 , sizeof( sensor_child_info_t ) * child_num );
		node_num++;
	}
	// init
	o->line.num = node_num;
	o->line.pa_sensor = ( sensor_info_t** )frizz_malloc( sizeof( sensor_info_t* )*node_num );
	if( o->line.pa_sensor == 0 ) {
		// malloc error
		return -3;
	}
	memset( o->line.pa_sensor, 0, sizeof( sensor_info_t* )*node_num );

	for( p_node = o->p_link; p_node != 0; p_node = p_node->p_next ) {
		if( ( p_node->info.status[SENSOR_AREA_0] & 0x80000000 ) == 0 ) {
			sub_linearize( o, p_node );
		}
	}
	o->line.num = node_num;

	return 0;
}

void sensor_list_destroy( sensor_list_h h )
{
	sensor_list_t* o = ( sensor_list_t* )h;
	if( o != 0 ) {
		struct node_t *p_node = o->p_link;
		while( p_node != 0 ) {
			struct node_t *swp = p_node->p_next;
			node_destroy( p_node );
			p_node = swp;
		}
		if( o->line.pa_sensor != 0 ) {
			frizz_free( o->line.pa_sensor );
		}
		frizz_free( o );
	}
}

sensor_list_h sensor_list_create( void )
{
	sensor_list_t* o = ( sensor_list_t* )frizz_malloc( sizeof( sensor_list_t ) );
	if( o != 0 ) {
		o->p_link = 0;
		o->line.pa_sensor = 0;
		o->line.num = 0;
		o->status = 0;
	}
	return o;
}

int sensor_list_add_sensor( sensor_list_h h, sensor_if_t *p_if )
{
	sensor_list_t* o = ( sensor_list_t* )h;
	struct node_t *p_node;

	if( o == 0 ) {
		// parameter error
		return -1;
	}

	if( o->status & 0x80000000 ) {
		// status error
		return -2;
	}

	p_node = node_create( p_if );
	if( p_node == 0 ) {
		// malloc error
		return -3;
	}

	// adding
	p_node->p_next = o->p_link;
	o->p_link = p_node;

	return 0;
}

int search_list_by_id( sensor_list_h h, unsigned char id )
{
	sensor_list_t* o = ( sensor_list_t* )h;
	struct node_t *top = o->p_link;
	unsigned char top_id;
	for( ; top != 0; top = top->p_next ) {
		top_id = top->info.p_if->get.id();
		if( top_id == id ) {
			return top_id;
		}
	}
	return 0;
}

static void remove_node_parent_not_exist( sensor_list_t* o )
{
	struct node_t *s_node;
	struct node_t *t_node;
	s_node = o->p_link;
	t_node = o->p_link;

	for( ; s_node != 0; ) {
		unsigned int i, par_num;
		unsigned char *par_ls;

		par_num = s_node->info.p_if->get.parent_list( &par_ls );
		for( i = 0; i < par_num; i++ ) {
			if( search_list_by_id( o, par_ls[i] ) == 0 ) {
				if( s_node == o->p_link ) {
					struct node_t *swp = s_node->p_next;
					if( s_node->info.p_if != 0 ) {
						set_device_condition_initerr( s_node->info.p_if->get.id() );
						DEBUG_PRINT( "@@1. delete sen_id=x%02X par_ls[%d]=x%02X\r\n", s_node->info.p_if->get.id(), i, par_ls[i] );
					}
					node_destroy( s_node );
					//s_node = swp;
					o->p_link = swp;
				} else {
					struct node_t *swp = s_node->p_next;
					if( s_node->info.p_if != 0 ) {
						set_device_condition_initerr( s_node->info.p_if->get.id() );
						DEBUG_PRINT( "@@2. delete sen_id=x%02X par_ls[%d]=x%02X\r\n", s_node->info.p_if->get.id(), i, par_ls[i] );
					}
					node_destroy( s_node );
					s_node = swp;
					t_node->p_next = swp;
				}
				s_node = o->p_link;

				i = par_num;
			} else {
				if( i == ( par_num - 1 ) ) {
					t_node = s_node;
					s_node = s_node->p_next;
				}
			}
		}
		if( par_num == 0 ) {
			t_node = s_node;
			s_node = s_node->p_next;
		}
	}
}

#ifdef	USE_ALIGMENT_CHECK
static void check_alignment_error( sensor_list_t* o )
{
	struct node_t *s_node = o->p_link;

	for( ; s_node != 0; s_node = s_node->p_next ) {
		if( s_node->info.p_if->get.data != 0 ) {
			void			*data;
			s_node->info.p_if->get.data( &data, 0 );
			if( ( ( unsigned long ) data & 0x0000000F ) != 0 ) {
				DEBUG_PRINT( "@@   alignment ng sen_id=x%02X\r\n", s_node->info.p_if->get.id() );
			} else {
				//DEBUG_PRINT("@@   alignment ok sen_id=x%02X\r\n",s_node->info.p_if->get.id());
			}
		}
	}
}
#endif

int sensor_list_fix( sensor_list_h h )
{
	sensor_list_t* o = ( sensor_list_t* )h;

	if( o == 0 ) {
		// parameter error
		return -1;
	}

	if( o->status & 0x80000000 ) {
		// status error
		return -2;
	}
	o->status |= 0x80000000;

#ifdef	USE_ALIGMENT_CHECK
	check_alignment_error( o );
#endif
	remove_node_parent_not_exist( o );

	connect_dependency( o->p_link );

	return sensor_list_linearize( o );
}

sensor_list_linear_t* sensor_list_get_linear_list( sensor_list_h h )
{
	sensor_list_t* o = ( sensor_list_t* )h;

	if( o != 0 ) {
		return &( o->line );
	} else {
		return 0;
	}
}

