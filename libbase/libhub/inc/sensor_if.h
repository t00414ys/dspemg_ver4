/*!******************************************************************************
 * @file  sensor_if.h
 * @brief sensor interface for sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __SENSOR_IF_H__
#define __SENSOR_IF_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @brief Updated status for callback
 */
typedef enum {
	SENSOR_NOTIFY_UPDATED_ACTIVATE=0,	/// Updated status of activation
	SENSOR_NOTIFY_UPDATED_INTERVAL,		/// Updated interval
	SENSOR_NOTIFY_UPDATED_DATA,			/// Updated value of data

	SENSOR_NOTIFY_DELIMITER				/// Just delimiter or count
} SENSOR_NOTIFY;

/**
 * @brief get interface for sensor
 */
typedef struct {
	/**
	 * @brief get ID of sensor
	 *
	 * @return sensor ID 0x00~0xFE
	 */
	unsigned char ( *id )( void );

	/**
	 * @brief get list about depending on other sensors
	 *
	 * @param [out] list pointer to list
	 *
	 * @return amount of entry
	 */
	unsigned int ( *parent_list )( unsigned char **list );

	/**
	 * @brief get status of sensor about active
	 *
	 * @return 0:not active, !0:active
	 */
	int ( *active )( void );

	/**
	 * @brief get current update interval
	 *
	 * @return interval number of tick count (-1: One-shot, 0: On-event, 0<: interval time)
	 */
	int ( *interval )( void );

	/**
	 * @brief get data from sensor
	 *
	 * @param [out] data pointer to data
	 * @param [out] ts timestamp
	 *
	 * @return amount of data entry
	 */
	int ( *data )( void** data, unsigned int *ts );

	/**
	 * @brief get necessity of calculation
	 *
	 * @return 0:no need, !0:need to call calculate();
	 */
	int ( *need_calc )( void );

	/**
	 * @brief get device condition(raw device)
	 *
	 * @return status
	 *
	 * @note note implementation is option , However, raw device is required
	 */
	unsigned int ( *condition )( void );

} sensor_if_get_t ;

/**
 * @brief set interface for sensor
 */
typedef struct {
	/**
	 * @brief set interface of sensor which on depending
	 *
	 * @param [in] gettor get interface
	 */
	void ( *parent_if )( sensor_if_get_t *gettor );

	/**
	 * @brief set status of sensor about active
	 *
	 * @param [in] f_active 0:not active, !0:active
	 */
	void ( *active )( int f_active );

	/**
	 * @brief set update interval
	 *
	 * @param [in] tick_num interval number of tick count
	 *
	 * @return current interval number of tick count (-1: One-shot, 0: On-event, 0<: interval time)
	 */
	int ( *interval )( int tick_num );
} sensor_if_set_t;

/**
 * @brief sensor interface
 */
typedef struct {
	sensor_if_get_t			get;	/// get interface (mandatory)
	sensor_if_set_t			set;	/// set interface (mandatory)

	/**
	 * @brief notify timestamp
	 *
	 * @param [in] ts current timestamp
	 *
	 * @param return timestamp for next wakeup
	 *
	 * @note note implementation is option
	 */
	unsigned int ( *notify_ts )( unsigned int ts );

	/**
	 * @brief notify updated timing from parent sensor
	 *
	 * @param [in] sen_id sensor ID of parent sensor
	 * @param [in] ntfy #SENSOR_NOTIFY
	 *
	 * @note note implementation is option
	 */
	void ( *notify_updated )( unsigned char sen_id, SENSOR_NOTIFY ntfy );

	/**
	 * @brief calculate for sensor
	 *
	 * @return 0:no update, !0: updated
	 *
	 * @note note implementation is option
	 */
	int ( *calculate )( void );

	/**
	 * @brief command interface for sensor
	 *
	 * @param [in] cmd_code code of command
	 * @param [in] param pointer to parameters
	 *
	 * @return response from sensor
	 *
	 * @note note implementation is option
	 */
	int ( *command )( unsigned int cmd_code, void *param );

	/**
	 * @brief end procedure for sensor
	 *
	 * @note note implementation is option
	 */
	void ( *end )( void );
} sensor_if_t;


/////
//
// frizz sensor condition define
//
#define	CONDITION_REGIST		(1  <<  0)							// device is registered with a initial table
#define	CONDITION_PHYNODEV		(1  <<  1)							// device is not registered  a initial table
#define	CONDITION_INITERR		(1  <<  2)							// device is initial error
#define	CONDITION_PHYERR		(1  <<  3)							// physical device error
#define	CONDITION_ACTIVE		(1  <<  4)							// device active
//
#define	CONDITION_PHYDEV		(1  <<  7)							// physical device


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
