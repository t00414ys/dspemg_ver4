/*!******************************************************************************
 * @file hubhal_out.h
 * @brief source code for hubhal_out.c
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUBHAL_OUT_H__
#define __HUBHAL_OUT_H__

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief output data information
 */
typedef struct {
	int		no;
	int 	default_level;
} hubhal_out_info_t;

extern hubhal_out_info_t g_hubhal_out;


/**
 * @brief initialize
 *
 * @param [in] no 0~n:using interrupt signal with GPIO#, <0: not use GPIO output signal
 */
void hubhal_out_init( int no, int level );

/**
 * @brief output
 *
 * @param [in] sen_id sensor ID
 * @param [in] area sensor area (0 or 1)
 * @param [in] ts timestamp[msec]
 * @param [in] num amount of words of data
 * @param [in] data data of words array
 * @param [in] f_block 0: non-blocking, !0: blocking
 * @param [in] f_notify 0: none, !0: notify with with interrupt signal on GPIO#
 *
 * @return 0: Success, <0: Fail
 *
 * @note GPIO# using with intterupt is set by #hubhal_out_init()
 */
int hubhal_out_snd_data( unsigned char sen_id, unsigned int area, unsigned int ts, unsigned int num, void *data, int f_block, int f_notify );

/**
 * @brief output response for command
 *
 * @param [in] sen_id sensor ID
 * @param [in] cmd_code CMD Code
 * @param [in] res Response Code
 */
void hubhal_out_res_cmd( unsigned char sen_id, unsigned int cmd_code, int res, unsigned int num, void *data );

/**
 * @brief ACK
 *
 * @param f_ack 0:NACK, !0:ACK
 *
 * @note only use in interrupt handler for critical section
 */
void hubhal_out_ack( int f_ack );

/**
 * @brief fifo cnt get
 *
 * @return fifo counter
 *
 */
int get_fifo_cnt( void );


#ifdef __cplusplus
}
#endif

#endif//__HUBHAL_OUT_H__
