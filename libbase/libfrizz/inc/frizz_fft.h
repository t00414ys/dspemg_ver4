/*!******************************************************************************
 * @file  frizz_fft.h
 * @brief for FFT on frizz
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_FFT_H__
#define __FRIZZ_FFT_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Structure for complex number
 */
typedef struct {
	frizz_fp	re;		///< Real
	frizz_fp	im;		///< Imaginary
} frizz_fft_com_t;

/**
 * @brief handler for FFT Lib
 */
typedef struct {
	int					tap_log;	///< tap_num = 2^tap_log
	int					tap_num;	///< num of points
	frizz_fft_com_t		*tbl;		///< coeff table
	frizz_fp			*window;	///< window function for stft
} frizz_fft_info_t;

/**
 * @brief Create FFT Handler
 *
 * @param tap_log number of points (= 2^tap_log)
 *
 * @return handler for FFT Lib
 */
frizz_fft_info_t *frizz_fft_create( int tap_log );

/**
 * @brief Destroy FFT Handler
 *
 * @param [in] info handler for FFT Lib
 */
void frizz_fft_destroy( frizz_fft_info_t *info );

/**
 * @brief FFT Transform for complex number data
 *
 * @param [in,out] info handler for FFT Lib
 * @param [in,out] data input complex number data
 */
void frizz_fft_transform( frizz_fft_info_t *info, frizz_fft_com_t *data );

/**
 * @brief FFT Inverse Transform
 *
 * @param [in,out] info handler for FFT Lib
 * @param [in,out] data input complex number data
 */
void frizz_fft_inverse( frizz_fft_info_t *info, frizz_fft_com_t *data );


#ifdef __cplusplus
}
#endif

#endif//__FRIZZ_FFT_H__
