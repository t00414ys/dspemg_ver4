/*!******************************************************************************
 * @file    quaternion_base.h
 * @brief   virtual quaternion_base sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#if	!defined(__QUATERNION_BASE_H__)
#define	__QUATERNION_BASE_H__
#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Create DCM (Rotation Matrix) from Quaternion
 *
 * @param [in] pq quaternion
 * @param [out] dcm[3] DCM
 */
void quaternion_base_create_dcm( frizz_fp4w_t *pq, frizz_fp4w_t dcm[3] );

/**
 * @brief Generate Base quaternion from Acceleromater and Magnetometer
 *
 * @param [out] pq quaternion
 * @param [in] pa accelerometer
 * @param [in] pm magnetometer
 */
void quaternion_base_am_generate( frizz_fp4w_t *pq, frizz_fp4w *pa, frizz_fp4w *pm );

/**
 * @brief Update quaternion with Gyroscope
 *
 * @param [in,out] pq quaternion
 * @param [in] pg gyroscope
 * @param [in] dt sampling period [sec]
 */
void quaternion_base_g_update( frizz_fp4w_t *pq, frizz_fp4w *pg, frizz_fp dt );

/**
 * @brief Update quaternion with Acceleromater and Magnetometer and Gyroscope
 *
 * @param [in,out] pq quaternion
 * @param [in] pa accelerometer
 * @param [in] pm magnetometer
 * @param [in] pg gyroscope
 * @param [in] dt sampling period [sec]
 */
void quaternion_base_amg_update( frizz_fp4w_t *pq, frizz_fp4w *pa, frizz_fp4w *pm, frizz_fp4w *pg, frizz_fp dt );

#ifdef __cplusplus
}
#endif

#endif


