/*!******************************************************************************
 * @file  frizz_math_common.c
 * @brief common define for frizz_math
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"

/// const number
static const float _frizz_math_const[] = {
	3.14159265358979323846,					/// pi
	3.14159265358979323846 * 2.0,				/// 2pi
	3.14159265358979323846 * 0.5,				/// pi/2
	3.14159265358979323846 * 0.25,			/// pi/4
	0.31830988618379067153776752674503,		/// 1/pi
	0.01745329251994329576923690768489,		/// pi/180
	57.295779513082320876798154814105,		/// 180/pi
	3.3554432000e+07,						/// 0x4c000000 as 2^25
};
const frizz_fp *frizz_math_const = ( const frizz_fp* )_frizz_math_const;

