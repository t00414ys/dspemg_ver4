/*!******************************************************************************
 * @file    sensor_statistics.c
 * @brief   source for statistics of sensor data
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math.h"
#include "sensor_statistics.h"

#define SENSOR_STAT_REFRESH_CNT (1 << 16)	// exponent bit is 16

/**
 * @brief initilize sensor_stat_t
 *
 * @param [in][out] h #sensor_stat_t
 * @param [in] buff ring buffer to calculate statistics value
 * @param [in] num ring buffer num
 *
 * @return 0: success, !0:not success
 */
int sensor_stat_init( sensor_stat_t *h, frizz_fp4w *buff, int num )
{
	// check param
	if( h == 0 || buff == 0 || num <= 0 ) {
		// parameter error
		return -1;
	} else {
		int i;
		// clear buffer
		for( i = 0; i < num; i++ ) {
			buff[i] = ( frizz_fp )0;
		}
		// for buffering
		h->buff.ring = buff;
		h->buff.v_sum_vec = ( frizz_fp )0;
		h->buff.v_sum_pow = ( frizz_fp )0;
		h->buff.s_sum_abs = ( frizz_fp )0;
		h->buff.coeff_avg = ( ( frizz_fp )1 ) / ( ( frizz_fp )num );
		h->buff.num = num;
		h->buff.idx = 0;
		h->buff.cnt_refresh = 0;
		h->buff.f_available = 0;
		// for result
		h->data.avg_vec = ( frizz_fp )0;
		h->data.var_com = ( frizz_fp )0;
		h->data.pow_avg = ( frizz_fp )0;
		h->data.var_abs = ( frizz_fp )0;
		// success
		return 0;
	}
}

/**
 * @brief push sensor data
 *
 * @param [in][out] h #sensor_stat_t
 * @param [in] d sensor data
 *
 * @return buffer status
 */
int sensor_stat_push_data( sensor_stat_t *h, const frizz_fp4w d )
{
	frizz_fp4w v, v_pow;
	frizz_fp s_pow, s_abs;
	int idx;
	if( h == 0 ) {
		return 0;
	}
	idx = h->buff.idx;
	if( h->buff.f_available != 0 ) {
		// pop oldest data
		v = h->buff.ring[idx];
		v_pow = v * v;
		s_pow = frizz_tie_vreduc( v_pow );
		s_abs = s_pow * frizz_inv_sqrt( s_pow );
		h->buff.v_sum_vec -= v;
		h->buff.v_sum_pow -= v_pow;
		h->buff.s_sum_abs -= s_abs;
		// count for refreshing
		h->buff.cnt_refresh++;
	}
	v = d;
	v_pow = v * v;
	s_pow = frizz_tie_vreduc( v_pow );
	s_abs = s_pow * frizz_inv_sqrt( s_pow );
	h->buff.v_sum_vec += v;
	h->buff.v_sum_pow += v_pow;
	h->buff.s_sum_abs += s_abs;
	// update index
	h->buff.ring[idx] = v;
	h->buff.idx++;
	if( h->buff.num <= h->buff.idx ) {
		// filled ring buffer up
		h->buff.idx = 0;
		h->buff.f_available = 1;
	}
	if( h->buff.f_available != 0 ) {
		frizz_fp4w v_avg_vec, v_avg_pow, v_pow_avg_vec;
		frizz_fp s_avg_abs;
		frizz_fp coeff = h->buff.coeff_avg;
		// for refreshing
		if( SENSOR_STAT_REFRESH_CNT <= h->buff.cnt_refresh ) {
			int i;
			h->buff.v_sum_vec = ( frizz_fp )0;
			h->buff.v_sum_pow = ( frizz_fp )0;
			h->buff.s_sum_abs = ( frizz_fp )0;
			for( i = 0; i < h->buff.num; i++ ) {
				v = h->buff.ring[i];
				v_pow = v * v;
				s_pow = frizz_tie_vreduc( v_pow );
				s_abs = s_pow * frizz_inv_sqrt( s_pow );
				h->buff.v_sum_vec += v;
				h->buff.v_sum_pow += v_pow;
				h->buff.s_sum_abs += s_abs;
			}
			h->buff.cnt_refresh = 0;
		}
		v_avg_vec = coeff * h->buff.v_sum_vec;
		v_avg_pow = coeff * h->buff.v_sum_pow;
		s_avg_abs = coeff * h->buff.s_sum_abs;
		h->data.avg_vec = v_avg_vec;
		v_pow_avg_vec = v_avg_vec * v_avg_vec;
		h->data.pow_avg = frizz_tie_vreduc( v_pow_avg_vec );
		h->data.var_com = v_avg_pow - v_pow_avg_vec;
		h->data.var_abs = frizz_tie_vreduc( v_avg_pow ) - s_avg_abs * s_avg_abs;
	}
	return h->buff.f_available;
}

