/*!******************************************************************************
 * @file quart.c
 * @brief Queue on UART code for DEBUG
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "uart.h"
#include "quart.h"
#include "tyny_sprintf.h"

#define QUART_FIFO_FULL_NUM 121

int quart_init( unsigned int core_hz, QUART_BAUDRATE_MODE mode, int f_lock )
{
	sUART_Setting set;
	int ret;
	// setting
	memset( &set, 0, sizeof( set ) );
	set.fifo_num = QUART_FIFO_FULL_NUM;
	set.parity = UART_ParityNone;
	set.stop = UART_StopBit1;
	set.data = UART_DataBit8;
	if( f_lock ) {
		set.flow = UART_FlowControlHardware;
	} else {
		set.flow = UART_FlowControlNone;
	}
	switch( mode ) {
	case QUART_BAUDRATE_1250000:
		set.baudrate = 1250000;
		break;
	case QUART_BAUDRATE_125000:
		set.baudrate = 125000;
		break;
	case QUART_BAUDRATE_115200:
		set.baudrate = 115200;
		break;
	case QUART_BAUDRATE_57600:
		set.baudrate = 57600;
		break;
	case QUART_BAUDRATE_9600:
		set.baudrate = 9600;
		break;
	default:
		set.baudrate = mode;
		break;
	}
	// init
	ret = uart_init( core_hz, &set, 0, 0 );	// none callback, none calback arg
	uart_lock();
	return ret;
}

static char ascii_tbl[] = {
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', 'a', 'b',
	'c', 'd', 'e', 'f',
};

static char ASCII_TBL[] = {
	'0', '1', '2', '3',
	'4', '5', '6', '7',
	'8', '9', 'A', 'B',
	'C', 'D', 'E', 'F',
};

int quart_in_pop( void )
{
	int data = 0;
	unsigned char ch = 0;
	while( ch != '\n' ) {
		unsigned int size;
		while( uart_get_status( &size ) & UART_LS_RX ) {
			int no;
			uart_get_data( &ch, 1 );
			if( ch == '\n' ) {
				break;
			} else {
				for( no = 0; no < sizeof( ascii_tbl ) / sizeof( unsigned char ); no++ ) {
					if( ch == ascii_tbl[no] ) {
						break;
					}
					if( ch == ASCII_TBL[no] ) {
						break;
					}
				}
				if( no != sizeof( ascii_tbl ) / sizeof( unsigned char ) ) {
					data <<= 4;
					data |= no;
				}
			}
		}
	}
	return data;
}

void quart_out_push( int data )
{
	int cnt, no, flag, size;
	unsigned char str[12];	// 0x????????\r\n
	unsigned char *p = str;

	// setting prefix 0x
	*p = '0';
	p++;
	*p = 'x';
	p++;
	flag = 0;
	size = 4;	// include CR+LF
	for( cnt = 28; cnt >= 0; cnt -= 4 ) {
		no = ( data >> cnt ) & 0x0F;
		if( no != 0 || flag || cnt == 0 ) {
			*p = ascii_tbl[no];
			p++;
			flag = 1;
			size++;
		}
	}
	*p = '\r';
	p++;
	*p = '\n';
	// p++;
	// send
	p = str;
	for( cnt = 0; cnt < size; cnt++ ) {
		while( uart_snd_data( p, 1 ) != 1 );
		p++;
	}
}

#define		D_OUT_BUFF_SIZE		(256)
void quart_out_raw( const char* form, ... )
{
	va_list args;
	unsigned int size;
	char buf[D_OUT_BUFF_SIZE];
	char *str;

	va_start( args, form );
	tiny_vsprintf( buf, ( char * ) form, args );
	//vsprintf(buf, form, args);
	va_end( args );

	size = strnlen( buf, D_OUT_BUFF_SIZE );
	str = buf;
	while( size != 0 ) {
		int num = uart_snd_data( ( unsigned char* )str, size );
		str += num;
		size -= num;
	}
}

void quart_wait_full()
{
	unsigned int num;
	do {
		uart_get_status( &num );
	} while( num < QUART_FIFO_FULL_NUM );
}

