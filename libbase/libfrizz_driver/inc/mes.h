/*!******************************************************************************
 * @file mes.h
 * @brief header for message register
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef MES_H
#define MES_H

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief mes register callback function
 *
 * @param init() arg
 * @param int message
 *
 * @return
 */
typedef void ( *mes_callback )( void*, unsigned int );

/**
 * @brief mes initialize
 *
 * @param pf callback function
 * @param arg callback function args
 */
void mes_init( mes_callback pf, void* arg );

/**
 * @brief mes end
 */
void mes_end( void );

/**
 * @brief get message data
 *
 * @return message data
 */
unsigned int mes_get_data( void );

#ifdef __cplusplus
}
#endif

#endif
