/*!******************************************************************************
 * @file    lsm6ds3_api.h
 * @brief   lsm6ds3 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __LSM6DS3_API_H__
#define __LSM6DS3_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int lsm6ds3_init( unsigned int param );
void lsm6ds3_ctrl_accl( int f_ena );
void lsm6ds3_ctrl_gyro( int f_ena );
unsigned int lsm6ds3_rcv_accl( unsigned int tick );
unsigned int lsm6ds3_rcv_gyro( unsigned int tick );
int lsm6ds3_conv_accl( frizz_fp data[3] );
int lsm6ds3_conv_gyro( frizz_fp data[4] );

int lsm6ds3_gyro_get_condition( void *data );
int lsm6ds3_accl_get_condition( void *data );

int lsm6ds3_setparam_accl( void *ptr );
int lsm6ds3_setparam_gyro( void *ptr );
unsigned int lsm6ds3_get_ver( void );
unsigned int lsm6ds3_get_name( void );

int lsm6ds3_accl_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif

#endif /*  */
