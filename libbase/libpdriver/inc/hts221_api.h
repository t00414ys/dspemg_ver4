/*!******************************************************************************
 * @file    hts221_api.h
 * @brief   hts221 sensor api header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LTS221_API_H__
#define __LTS221_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int hts221_init( unsigned int param );
void hts221_ctrl( int f_ena );
unsigned int hts221_rcv( unsigned int tick );
int hts221_conv( frizz_fp* press_data );

int hts221_setparam( void *ptr );
unsigned int hts221_get_ver( void );
unsigned int hts221_get_name( void );

int hts221_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif // __LTS221_API_H__
