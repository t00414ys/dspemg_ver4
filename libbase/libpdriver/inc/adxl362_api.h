/*!******************************************************************************
 * @file    adxl362_api.h
 * @brief   adxl362 sensor api header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADXL362_API_H__
#define __ADXL362_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int adxl362_init( unsigned int param );
void adxl362_ctrl_accl( int f_ena );
unsigned int adxl362_rcv_accl( unsigned int tick );
int adxl362_conv_accl( frizz_fp data[3] );

int adxl362_setparam_accl( void *ptr );
unsigned int adxl362_get_ver( void );
unsigned int adxl362_get_name( void );

int adxl362_get_condition( void *data );
int adxl362_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif


#endif

