/*!******************************************************************************
 * @file    icm20601_api.h
 * @brief   icm20601 sensor api header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ICM20601_API_H__
#define __ICM20601_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int icm20601_init( unsigned int param );
void icm20601_ctrl_magn( int f_ena );
unsigned int icm20601_rcv_magn( unsigned int tick );
int icm20601_conv_magn( frizz_fp data[3] );

int icm20601_setparam_magn( void *ptr );
unsigned int icm20601_get_ver( void );
unsigned int icm20601_get_name( void );

int icm20601_get_condition( void *data );
int icm20601_get_raw_data( void *data );


#ifdef __cplusplus
}
#endif


#endif
