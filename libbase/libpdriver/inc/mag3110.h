/*!******************************************************************************
 * @file    mag3110.h
 * @brief   mag3110 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 * @par     Data Sheet
 *          MC3413 3-Axis Accelerometer Data Sheet APS-048-0029v1.7
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAG3110_H__
#define __MAG3110_H__
#include "mag3110_api.h"

#define MAG3110_SCALE            ((float)(1.)) //+-1000ut resolution 16bit

/*
 *
 *		By environment, you must edit this file
 *
 */
// driver parameter
#define MAG3110_ADDRESS		     (0x0E)	// I2C device address

/*  register map*/
#define MAG3110_DR_STATUS		 (0x00) // data ready status per axis
#define MAG3110_OUT_X_MSB		 (0x01) // Bits [15:8] of X measurement
#define MAG3110_OUT_X_LSB		 (0x02) // Bits [ 7:0] of X measurement
#define MAG3110_OUT_Y_MSB		 (0x03) // Bits [15:8] of Y measurement
#define MAG3110_OUT_Y_LSB		 (0x04) // Bits [ 7:0] of Y measurement
#define MAG3110_OUT_Z_MSB		 (0x05) // Bits [15:8] of Z measurement
#define MAG3110_OUT_Z_LSB		 (0x06) // Bits [ 7:0] of Z measurement

#define MAG3110_SYSMOD	  	     (0x08) // Current System Mode
#define MAG3110_OFF_X_MSB		 (0x09) // Bits [14:7] of user X offset
#define MAG3110_OFF_X_LSB		 (0x0A) // Bits [ 6:0] of user X offset
#define MAG3110_OFF_Y_MSB		 (0x0B) // Bits [14:7] of user Y offset
#define MAG3110_OFF_Y_LSB		 (0x0C) // Bits [ 6:0] of user Y offset
#define MAG3110_OFF_Z_MSB		 (0x0D) // Bits [14:7] of user Z offset
#define MAG3110_OFF_Z_LSB		 (0x0E) // Bits [ 6:0] of user Z offset
#define MAG3110_DIE_TEMP	  	 (0x0F) // Temperature, signed 8 bits in C

// state
#define MAG3110_CTRL_REG1        (0x10) // Operation modes
#define MAG3110_CTRL_REG2        (0x11) // Operation modes
#define MAG3110_MODE_STANDBY     (0x00) // standby mode
#define MAG3110_MODE_ACTIVE      (0x01) // active mode

// product code
#define MAG3110_WHO_AM_I		 (0x07) // Device identification register
#define MAG3110_WHO_AM_I_ID      (0xC4) // Device ID Number

// sample rate
#define MAG3110_DATA_RATE        (0x01) // 80Hz [0000 0001]

// automatic magnetic sensor resets
#define MAG3110_AUTO_MRST_EN	(0x80) // enable

#endif

