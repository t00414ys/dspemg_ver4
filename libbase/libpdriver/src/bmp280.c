/*!******************************************************************************
 * @file    bmp280.c
 * @brief   bmp280 sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <string.h>
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "bmp280.h"
#include "config_type.h"

#define		D_DRIVER_NAME_P			D_DRIVER_NAME_BMP280
#define		D_DRIVER_NAME_E			D_DRIVER_NAME_BME280


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(0)						// Minor Version
#define		DRIVER_VER_DETAIL		(4)						// Detail Version

typedef	signed short		BME280_S16_t;
typedef	signed long			BMP280_S32_t;
typedef	unsigned long		BMP280_U32_t;
typedef	signed long long	BMP280_S64_t;
typedef	unsigned long long	BMP280_U64_t;

struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[3];	// transmission buffer
	unsigned char		ctrl;
	unsigned char		addr;
	unsigned char		device_id;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[1];
	//
} g_press_bmp;

struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[2];	// transmission buffer
	unsigned char		ctrl;
	unsigned char		addr;
	unsigned char		device_id;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[1];
	//
} g_rh_bme;		// relative humidity

struct {
	unsigned char		buff[3];	// transmission buffer
} g_temp_bmp;

struct {
	unsigned char		buff[24];	// transmission buffer
	unsigned short		dig_T1;
	signed short		dig_T2, dig_T3;
	unsigned short		dig_P1;
	signed short		dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9, dig_H2, dig_H4, dig_H5;
	unsigned char 		dig_H1, dig_H3;
	signed char			dig_H6;

	BMP280_S32_t 		t_fine;
} g_trimming_bmx280;


/*	breif description:
* Returns temperature in DegC, resolution is 0.01 DegC. Output value of �g5123�h equals 51.23 DegC.
* "t_fine" carries fine temperature as global value
*/
BMP280_S32_t bmp280_compensate_T_int32( BMP280_S32_t adc_t )
{
	BMP280_S32_t	v_x1_u32r = 0;
	BMP280_S32_t	v_x2_u32r = 0;
	BMP280_S32_t	temperature = 0;

	v_x1_u32r  = ( ( ( ( adc_t >> 3 ) - ( ( BMP280_S32_t ) g_trimming_bmx280.dig_T1 << 1 ) ) ) * ( ( BMP280_S32_t )g_trimming_bmx280.dig_T2 ) ) >> 11;
	v_x2_u32r  = ( ( ( ( ( adc_t >> 4 ) - ( ( BMP280_S32_t )g_trimming_bmx280.dig_T1 ) ) * ( ( adc_t >> 4 ) -
					   ( ( BMP280_S32_t )g_trimming_bmx280.dig_T1 ) ) ) >> 12 ) * ( ( BMP280_S32_t )g_trimming_bmx280.dig_T3 ) ) >> 14;
	g_trimming_bmx280.t_fine = v_x1_u32r + v_x2_u32r;
	temperature  = ( g_trimming_bmx280.t_fine * 5 + 128 ) >> 8;

	return temperature;
}

/*	breif description:
* Returns pressure in Pa as unsigned 32bit integer in Q24.8 format(24 integer 8 fractional bits).
*	An output value of "24674867" represents 24674867 / 256 = 96386.2 Pa = 963.862 hPa
*/
BMP280_U32_t bmp280_compensate_P_int64( BMP280_S32_t adc_p )
{
	BMP280_S64_t	v_x1_s64r = 0;
	BMP280_S64_t	v_x2_s64r = 0;
	BMP280_S64_t	pressure  = 0;

	v_x1_s64r = ( ( BMP280_S64_t )g_trimming_bmx280.t_fine ) - 128000;
	v_x2_s64r = v_x1_s64r * v_x1_s64r * ( BMP280_S64_t )g_trimming_bmx280.dig_P6;
	v_x2_s64r = v_x2_s64r + ( ( v_x1_s64r * ( BMP280_S64_t )g_trimming_bmx280.dig_P5 ) << 17 );
	v_x2_s64r = v_x2_s64r + ( ( ( BMP280_S64_t )g_trimming_bmx280.dig_P4 ) << 35 );
	v_x1_s64r = ( ( v_x1_s64r * v_x1_s64r * ( BMP280_S64_t )g_trimming_bmx280.dig_P3 ) >> 8 ) + ( ( v_x1_s64r * ( BMP280_S64_t )g_trimming_bmx280.dig_P2 ) << 12 );
	v_x1_s64r = ( ( ( ( ( BMP280_S64_t )1 ) << 47 ) + v_x1_s64r ) ) * ( ( BMP280_S64_t )g_trimming_bmx280.dig_P1 ) >> 33;
	if( v_x1_s64r == 0 ) {
		return 0; /* Avoid exception caused by division by zero */
	}
	pressure = 1048576 - adc_p;
	pressure = ( ( ( pressure << 31 ) - v_x2_s64r ) * 3125 ) / v_x1_s64r;
	v_x1_s64r = ( ( ( BMP280_S64_t )g_trimming_bmx280.dig_P9 ) * ( pressure >> 13 ) * ( pressure >> 13 ) ) >> 25;
	v_x2_s64r = ( ( ( BMP280_S64_t )g_trimming_bmx280.dig_P8 ) * pressure ) >> 19;
	pressure = ( ( pressure + v_x1_s64r + v_x2_s64r ) >> 8 ) + ( ( ( BMP280_S64_t )g_trimming_bmx280.dig_P7 ) << 4 );
	return ( BMP280_U32_t )pressure;
}

/* breif description:
*	Returns humidity in %rH as unsigned 32bit integer in Q22.10 format(22 integer 10 fractional bits).
*	An output value of 42313 represents 42313 / 1024 = 42.313 %rH
*/

BMP280_U32_t bme280_compensate_H_int32( BMP280_S32_t adc_h )
{
	BMP280_S32_t v_x1_u32r;
	v_x1_u32r = ( g_trimming_bmx280.t_fine - ( ( BMP280_S32_t )76800 ) );
	v_x1_u32r = ( ( ( ( ( adc_h << 14 ) -	( ( ( BMP280_S32_t )g_trimming_bmx280.dig_H4 ) << 20 ) -
						( ( ( BMP280_S32_t )g_trimming_bmx280.dig_H5 ) * v_x1_u32r ) ) +	( ( BMP280_S32_t )16384 ) ) >> 15 ) *
				  ( ( ( ( ( ( ( v_x1_u32r *	( ( BMP280_S32_t )g_trimming_bmx280.dig_H6 ) ) >> 10 ) *	( ( ( v_x1_u32r * ( ( BMP280_S32_t )g_trimming_bmx280.dig_H3 ) ) >> 11 ) +
							( ( BMP280_S32_t )32768 ) ) ) >> 10 ) +	( ( BMP280_S32_t )2097152 ) ) *	( ( BMP280_S32_t )g_trimming_bmx280.dig_H2 ) + 8192 ) >> 14 ) );
	v_x1_u32r = ( v_x1_u32r - ( ( ( ( ( v_x1_u32r >> 15 ) * ( v_x1_u32r >> 15 ) ) >> 7 ) *	( ( BMP280_S32_t )g_trimming_bmx280.dig_H1 ) ) >> 4 ) );
	v_x1_u32r = ( v_x1_u32r < 0 ? 0 : v_x1_u32r );
	v_x1_u32r = ( v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r );
	return ( BMP280_U32_t )( v_x1_u32r >> 12 );
}

void bmx280_trimming( int BME_or_BMP )
{
	unsigned char addr;

	if( BME_or_BMP == 1 ) {
		addr = g_press_bmp.addr;
	} else {
		addr = g_rh_bme.addr;
	}

	i2c_read( addr, BMP_REG_TRIMMING, g_trimming_bmx280.buff, sizeof( g_trimming_bmx280.buff ) );

	g_trimming_bmx280.dig_T1 = *( unsigned short * )&g_trimming_bmx280.buff[0];
	g_trimming_bmx280.dig_T2 = *( short * )&g_trimming_bmx280.buff[2];
	g_trimming_bmx280.dig_T3 = *( short * )&g_trimming_bmx280.buff[4];
	g_trimming_bmx280.dig_P1 = *( unsigned short * )&g_trimming_bmx280.buff[6];
	g_trimming_bmx280.dig_P2 = *( short * )&g_trimming_bmx280.buff[8];
	g_trimming_bmx280.dig_P3 = *( short * )&g_trimming_bmx280.buff[10];
	g_trimming_bmx280.dig_P4 = *( short * )&g_trimming_bmx280.buff[12];
	g_trimming_bmx280.dig_P5 = *( short * )&g_trimming_bmx280.buff[14];
	g_trimming_bmx280.dig_P6 = *( short * )&g_trimming_bmx280.buff[16];
	g_trimming_bmx280.dig_P7 = *( short * )&g_trimming_bmx280.buff[18];
	g_trimming_bmx280.dig_P8 = *( short * )&g_trimming_bmx280.buff[20];
	g_trimming_bmx280.dig_P9 = *( short * )&g_trimming_bmx280.buff[22];

	if( BME_or_BMP == 1 ) {
		return;
	}
	i2c_read( addr, BME_REG_TRIMMING_H0, &g_trimming_bmx280.buff[0], 1 );
	g_trimming_bmx280.dig_H1 = g_trimming_bmx280.buff[0];

	i2c_read( addr, BME_REG_TRIMMING_H1, &g_trimming_bmx280.buff[0], 8 );

	g_trimming_bmx280.dig_H2 = ( BME280_S16_t )( ( ( ( BME280_S16_t )( ( signed char )g_trimming_bmx280.buff[1] ) ) << 8 ) | g_trimming_bmx280.buff[0] );
	g_trimming_bmx280.dig_H3 = g_trimming_bmx280.buff[2];
	g_trimming_bmx280.dig_H4 = ( BME280_S16_t )( ( ( ( BME280_S16_t )( ( signed char )g_trimming_bmx280.buff[3] ) ) << 4 ) | ( ( ( unsigned char )0x0F )	& g_trimming_bmx280.buff[4] ) );
	g_trimming_bmx280.dig_H5 = ( BME280_S16_t )( ( ( ( BME280_S16_t )( ( signed char )g_trimming_bmx280.buff[5] ) ) << 4 ) | ( g_trimming_bmx280.buff[4] >>	4 ) );
	g_trimming_bmx280.dig_H6 = ( signed char )g_trimming_bmx280.buff[6];

}
#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;
static unsigned char		g_init_done_E = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	PRES_BMP_I2C_ADDRESS_L,
	PRES_BMP_I2C_ADDRESS_H,
};

int bmp280_init( unsigned int param )
{
	int					ret, i;
	unsigned char		data;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/*
	 * Ultra low power : TEMP_OVER*1 + PRES_OVER*1, 	ODR = 181.8Hz
	 * 		 low power : TEMP_OVER*1 + PRES_OVER*2, 	ODR = 133.3Hz
	 * 		  Standard : TEMP_OVER*1 + PRES_OVER*4, 	ODR = 87.0Hz
	 * High resolution : TEMP_OVER*1 + PRES_OVER*8, 	ODR = 51.3Hz
	 * Ultra high      : TEMP_OVER*2 + PRES_OVER*16, 	ODR = 37.5Hz
	 */
	g_press_bmp.ctrl = FORCED_MODE | TEMP_OVERSAMPLING_01 | PRES_OVERSAMPLING_04;

	g_press_bmp.scale = as_frizz_fp( BMP_PRESS_PER_LSB );
	/* recognition */

	// BMP280
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_press_bmp.addr =  add_tbl[i];
		g_press_bmp.device_id = 0;			// read id
		ret = i2c_read( g_press_bmp.addr, BMP_REG_ID, &g_press_bmp.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( ( g_press_bmp.device_id == BMP_WHOAMI_ID ) || ( g_press_bmp.device_id == BME_WHOAMI_ID ) ) {
				break;
			}
		}
	}

	if( ( g_press_bmp.device_id != BMP_WHOAMI_ID ) && ( g_press_bmp.device_id != BME_WHOAMI_ID ) ) {
		return RESULT_ERR_INIT;
	}
	// @@

	// osrs_t 001 (x1) osrs_p 001 (x1) mode = 01 or 10 (Forced mode)
	data      = SLEEP_MODE | TEMP_OVERSAMPLING_01 | PRES_OVERSAMPLING_04;				// 0xF4 00100111
	ret = i2c_write( g_press_bmp.addr, BMP_REG_CTRL_MEAS, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	data      = IIR_FILTER_4;
	ret = i2c_write( g_press_bmp.addr, BMP_REG_CONFIG, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	bmx280_trimming( 1 );

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void bmp280_ctrl( int f_ena )
{
	unsigned char		data;
	unsigned char		buff;

	if( f_ena ) {
		data      = g_press_bmp.ctrl;
		i2c_write( g_press_bmp.addr, BMP_REG_CTRL_MEAS, &data, 1 );
	} else {
		data = 0;
		i2c_read( g_press_bmp.addr, BME_REG_CTRLHUM, &data, 1 );
		buff = data & 0x7;			// humid oversampling
		if( buff == 0 ) {
			data      = SLEEP_MODE;				// three get into SLEEP
			i2c_write( g_press_bmp.addr, BMP_REG_CTRL_MEAS, &data, 1 );
		} else {
			data = 0;			// close temp and pres oversampling
			i2c_read( g_press_bmp.addr, BMP_REG_CTRL_MEAS, &data, 1 );
			data &= 0x3;		// backup mode[0:1]
			i2c_write( g_press_bmp.addr, BMP_REG_CTRL_MEAS, &data, 1 );
		}
	}
}

unsigned int bmp280_rcv( unsigned int tick )
{
	unsigned char		data;

	/*	If necessary, use it.
		i2c_read(g_press_bmp.addr, BMP_REG_STATUS, &data, 1);		// check status bit3: (1)running. (0) done
		if(data & 0x08)
		{
			g_press_bmp.recv_result = 1;
			return 0;
		}
	*/
	g_press_bmp.recv_result = i2c_read( g_press_bmp.addr, BMP_REG_PRESS, &g_press_bmp.buff[0], 3 );
	if( g_press_bmp.recv_result == 0 ) {
		g_press_bmp.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_press_bmp.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	i2c_read( g_press_bmp.addr, BMP_REG_TEMP, &g_temp_bmp.buff[0], 3 );

	// set Forced mode again for updating data.
	data      = g_press_bmp.ctrl;
	i2c_write( g_press_bmp.addr, BMP_REG_CTRL_MEAS, &data, 1 );
	return 0;
}

int bmp280_conv( frizz_fp* press_data )
{
	frizz_fp			fz;
	float				*fp = ( float* )&fz;

	BMP280_S32_t	adc_P;
	BMP280_S32_t	adc_T;
	BMP280_U32_t	adc_P_fix;

	if( g_press_bmp.recv_result != 0 ) {
		*press_data = g_press_bmp.lasttime_data[0];
		return RESULT_SUCCESS_CONV;
	}


	adc_P = ( BMP280_S32_t )( ( ( ( BMP280_U32_t )( g_press_bmp.buff[0] ) ) << 12 ) |
							  ( ( ( BMP280_U32_t )( g_press_bmp.buff[1] ) ) << 4 ) |
							  ( ( BMP280_U32_t ) g_press_bmp.buff[2]   >> 4 ) );

	adc_T = ( BMP280_S32_t )( ( ( ( BMP280_U32_t )( g_temp_bmp.buff[0] ) ) << 12 ) |
							  ( ( ( BMP280_U32_t )( g_temp_bmp.buff[1] ) ) << 4 )  |
							  ( ( BMP280_U32_t )  g_temp_bmp.buff[2]   >> 4 ) );

	bmp280_compensate_T_int32( adc_T );
	adc_P_fix = bmp280_compensate_P_int64( adc_P );
	if( adc_P_fix == 0 ) {
		return RESULT_ERR_CONV; // avoid exception caused by division by zero
	}

	fp[0] = ( float )adc_P_fix;
	*press_data = fz * g_press_bmp.scale;
	//
	g_press_bmp.lasttime_data[0] = *press_data;
	return RESULT_SUCCESS_CONV;
}


int bmp280_setparam( void *ptr )
{
	return RESULT_ERR_SET;
}

int bmp280_get_condition( void *data )
{
	return g_press_bmp.device_condition;
}

unsigned int bmp280_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int bmp280_get_name()
{
	return	D_DRIVER_NAME_P;
}

int bme280_init( unsigned int param )
{
	int					ret, i;
	unsigned char		data;

	if( g_init_done_E != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	g_rh_bme.ctrl = HUMID_OVERSAMPLING_04;

	g_rh_bme.scale = as_frizz_fp( BME_HUMID_PER_LSB );
	/* recognition */

	// BME280
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_rh_bme.addr =  add_tbl[i];
		g_rh_bme.device_id = 0;			// read id
		ret = i2c_read( g_rh_bme.addr, BMP_REG_ID, &g_rh_bme.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( g_rh_bme.device_id == BME_WHOAMI_ID ) {
				break;
			}
		}
	}

		if( g_rh_bme.device_id != BME_WHOAMI_ID ) {
			return RESULT_ERR_INIT;
		}

	data      = g_rh_bme.ctrl;
	ret = i2c_write( g_rh_bme.addr, BME_REG_HUMIDITY, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	i2c_read( g_rh_bme.addr, BMP_REG_CTRL_MEAS, &data, 1 );
	data &= 0xFC;
	data |= SLEEP_MODE;
	ret = i2c_write( g_rh_bme.addr, BMP_REG_CTRL_MEAS, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	data      = IIR_FILTER_4;
	ret = i2c_write( g_rh_bme.addr, BMP_REG_CONFIG, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	bmx280_trimming( 0 );

	g_init_done_E = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}


void bme280_ctrl( int f_ena )
{
	unsigned char		data;
	unsigned char		buff;

	if( f_ena ) {
		data = 0;
		i2c_read( g_rh_bme.addr, BME_REG_CTRLHUM, &data, 1 );
		buff = data & 0x03;			// mode[0:1]
		if( buff != FORCED_MODE ) {
			data = buff | FORCED_MODE;
			i2c_write( g_rh_bme.addr, BME_REG_CTRLHUM, &data, 1 );
		} else {
			data      = g_rh_bme.ctrl;
			i2c_write( g_rh_bme.addr, BME_REG_HUMIDITY, &data, 1 );
		}
	} else {
		data = 0;
		i2c_read( g_rh_bme.addr, BMP_REG_CTRL_MEAS, &data, 1 );
		buff = data & 0xFC;
		if( buff == 0 ) {
			data      = SLEEP_MODE;				// three get into SLEEP
			i2c_write( g_rh_bme.addr, BMP_REG_CTRL_MEAS, &data, 1 );
		} else {
			data = 0;			// close HUMID oversampling
			i2c_write( g_rh_bme.addr, BME_REG_CTRLHUM, &data, 1 );
		}
	}
}

unsigned int bme280_rcv( unsigned int tick )
{
	unsigned char		data;

	/*	If necessary, use it.
		i2c_read(g_rh_bme.addr, BMP_REG_STATUS, &data, 1);		// check status bit3: (1)running. (0) done
		if(data & 0x08)
		{
			g_rh_bme.recv_result = 1;
			return 0;
		}
	*/
	g_rh_bme.recv_result = i2c_read( g_rh_bme.addr, BME_REG_HUMIDITY, &g_rh_bme.buff[0], 2 );
	if( g_rh_bme.recv_result == 0 ) {
		g_rh_bme.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_rh_bme.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	i2c_read( g_rh_bme.addr, BMP_REG_TEMP, &g_temp_bmp.buff[0], 3 );

	// set Forced mode again for updating data.
	data      = g_rh_bme.ctrl;
	i2c_write( g_rh_bme.addr, BMP_REG_CTRL_MEAS, &data, 1 );
	return 0;

}

int bme280_conv( frizz_fp* humid_data )
{
	frizz_fp			fz;
	float				*fp = ( float* )&fz;

	BMP280_S32_t	adc_H;
	BMP280_S32_t	adc_T;
	BMP280_U32_t	adc_H_fix;

	if( g_rh_bme.recv_result != 0 ) {
		*humid_data = g_rh_bme.lasttime_data[0];
		return RESULT_SUCCESS_CONV;
	}


	adc_H = ( BMP280_S32_t )( ( ( ( BMP280_U32_t )( g_rh_bme.buff[0] ) ) << 8 ) |
							  ( ( BMP280_U32_t )( g_rh_bme.buff[1] ) ) );

	adc_T = ( BMP280_S32_t )( ( ( ( BMP280_U32_t )( g_temp_bmp.buff[0] ) ) << 12 ) |
							  ( ( ( BMP280_U32_t )( g_temp_bmp.buff[1] ) ) << 4 )  |
							  ( ( BMP280_U32_t )  g_temp_bmp.buff[2]   >> 4 ) );

	bmp280_compensate_T_int32( adc_T );
	adc_H_fix = bme280_compensate_H_int32( adc_H );

	if( adc_H_fix == 0 ) {
		return RESULT_ERR_CONV; // avoid exception caused by division by zero
	}

	fp[0] = ( float )adc_H_fix;
	*humid_data = fz * g_rh_bme.scale;
	//
	g_rh_bme.lasttime_data[0] = *humid_data;
	return RESULT_SUCCESS_CONV;
}

int bme280_setparam( void *ptr )
{
	return RESULT_ERR_SET;
}

int bme280_get_condition( void *data )
{
	return g_rh_bme.device_condition;
}

unsigned int bme280_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int bme280_get_name()
{
	return	D_DRIVER_NAME_E;
}
