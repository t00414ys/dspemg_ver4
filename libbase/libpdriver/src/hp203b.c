/*!******************************************************************************
 * @file    hp203b.c
 * @brief   hp203b sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "hp203b.h"
#include "config_type.h"
#include "timer.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_HP203B


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(0)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

//#define	USE_ALTITUDE

struct {
	//frizz_fp			scale;		// scaler
	unsigned char		buff[9];	// transmission buffer
	unsigned char		addr;
	unsigned char		device_id;
	unsigned char		adc_cvt;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data;
	//
} g_press_hp20x;

static unsigned char	add_tbl[] = {
	hp20x_I2C_ADDRESS_H,
	hp20x_I2C_ADDRESS_L,
};

int hp203b_i2c_write_cmd(unsigned char bus_addr, unsigned char cmd_addr)
{
	unsigned char buff;
	int ret;
	ret = i2c_write( bus_addr, cmd_addr, &buff, 0);
	return ret;
}

int hp203b_i2c_write_reg(unsigned char bus_addr, unsigned char reg_addr, unsigned char *buff, char length)
{
	int ret;
	reg_addr = reg_addr | 0xC0;   //ref to  spec p.10
	ret = i2c_write(bus_addr, reg_addr, buff, length);
	return ret;
}

int hp203b_i2c_read_reg(unsigned char bus_addr, unsigned char reg_addr, unsigned char *buff, char length)
{
	int ret;
	reg_addr = reg_addr | 0x80;   //ref to  spec p.10
	ret = i2c_read(bus_addr, reg_addr, buff, length);
	return ret;
}

int hp203b_init( unsigned int param )
{
	unsigned char buff;
	int ret,i,count;
	
	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	// Get Device ID
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_press_hp20x.addr =  add_tbl[i];
		g_press_hp20x.device_id = 0;			// read id
		ret = hp203b_i2c_read_reg( g_press_hp20x.addr, hp20x_WHO_AM_I, &g_press_hp20x.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( ( g_press_hp20x.device_id == HP203B_DEVICE_ID_1 ) || ( g_press_hp20x.device_id == HP203B_DEVICE_ID_2 ) ) {
				break;
			}
		}
	}
	if( ( g_press_hp20x.device_id != HP203B_DEVICE_ID_1 ) && ( g_press_hp20x.device_id != HP203B_DEVICE_ID_2 ) ) {
		return RESULT_ERR_INIT;
	}

	ret = hp203b_i2c_write_cmd( g_press_hp20x.addr, hp20x_REG_RESET_CMD);
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}
#if 0
	mdelay(10);
#else
	count = 10;
	while(count--)
	{
		buff = 0x00;
		mdelay(2);
		hp203b_i2c_read_reg( g_press_hp20x.addr, hp20x_REG_INT_SRC, &buff, 1);
		if(buff & 0x40)
			break;

	}
	if((buff != 0x40)) {
		return RESULT_ERR_INIT;
	}
#endif
	// OSR setting
	g_press_hp20x.adc_cvt = (hp20x_CONVERSION_REG | (OSR_256<<HP203B_ODR_BIT) | CHNL_PT);
	hp203b_i2c_write_cmd( g_press_hp20x.addr, g_press_hp20x.adc_cvt);

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void hp203b_ctrl( int f_ena )
{
	// do nothing.
}


unsigned int hp203b_rcv( unsigned int tick )
{
	//cmd.0x10:read temp and pressure
	hp203b_i2c_write_cmd( g_press_hp20x.addr, hp20x_TEMP_PRESSURE_MEASUREMENT );

	g_press_hp20x.recv_result = hp203b_i2c_read_reg( g_press_hp20x.addr, 0xFF, g_press_hp20x.buff, 6 );

#if defined(USE_ALTITUDE)
	mdelay(2);

	//cmd.0x31:read altitude
	hp203b_i2c_write_cmd( g_press_hp20x.addr, hp20X_ALTITUDE_MEASUREMNET );

	g_press_hp20x.recv_result = hp203b_i2c_read_reg( g_press_hp20x.addr, 0xFF, &g_press_hp20x.buff[6], 3 );
#endif	
	if( g_press_hp20x.recv_result == 0 ) {
		g_press_hp20x.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_press_hp20x.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	hp203b_i2c_write_cmd( g_press_hp20x.addr, g_press_hp20x.adc_cvt);
	return 0;
}

int hp203b_conv( frizz_fp* press_data )
{
	frizz_fp fz;
	float* fp = ( float* )&fz;
	unsigned int	sensor_buff[3];
	
	if( g_press_hp20x.recv_result != 0 ) {
		*press_data = g_press_hp20x.lasttime_data;
		return RESULT_SUCCESS_CONV;
	}

	// press value MSB 20bit
	sensor_buff[0] = ( ( ( unsigned int ) g_press_hp20x.buff[3] << 16 ) |
					( ( unsigned int ) g_press_hp20x.buff[4] <<  8 ) |
					( ( unsigned int ) g_press_hp20x.buff[5] <<  0 ) );

	// temperature value MSB 20bit
	sensor_buff[1] = ( ( ( unsigned int ) g_press_hp20x.buff[0] << 16 ) |
					( ( unsigned int ) g_press_hp20x.buff[1] <<  8 ) |
					( ( unsigned int ) g_press_hp20x.buff[2] <<  0 ) );

#if defined(USE_ALTITUDE)
	// altitude value MSB 20bit
	sensor_buff[2] = ( ( ( unsigned int ) g_press_hp20x.buff[6] << 16 ) |
					( ( unsigned int ) g_press_hp20x.buff[7] <<  8 ) |
					( ( unsigned int ) g_press_hp20x.buff[8] <<  0 ) );
#endif
	fp[0] = ( float )sensor_buff[0];

	// Hpa
	*press_data = fz * as_frizz_fp( HP203B_SCALE );
	//
	g_press_hp20x.lasttime_data = *press_data;

	return RESULT_SUCCESS_CONV;
}

int hp203b_setparam( void *ptr )
{
	return RESULT_ERR_SET;
}

int hp203b_get_condition( void *data )
{
	return g_press_hp20x.device_condition;
}

unsigned int hp203b_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int hp203b_get_name()
{
	return	D_DRIVER_NAME;
}

