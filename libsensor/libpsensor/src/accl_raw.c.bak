/*!******************************************************************************
 * @file    accl_raw.c
 * @brief   sample program for control accel raw data
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "sensor_if.h"
#include "accl_driver.h"
#include "if/accl_raw_if.h"
#include "frizz_math.h"
#include "frizz_const.h"
#include "gpio.h"

#define DEF_INIT(x) x ## _init

EXTERN_C sensor_if_t* DEF_INIT( accl_raw )( void );

/* accel phy sensor list */
static	pdriver_if_t	g_devif[] = {
		ADS1191_DATA					// ADS1191
};
static	pdriver_if_t	*g_pDevIF;

typedef struct {
	// ID
	unsigned char		id;
	// IF
	sensor_if_t			pif;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	unsigned int		remain_total;
	// data
	frizz_fp			data[7];	//return するデータ数
} device_sensor_t;

static device_sensor_t g_device;
accel_raw_data_t accel_data;
unsigned int	g_accel_name = 0;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	return 0;
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 7;	//data[?]の?にそろえる
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		if( g_pDevIF->ctrl != 0 ) {
			( *g_pDevIF->ctrl )( f_active );
		}
		g_device.f_active = f_active;
		g_device.remain_total = 0;
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	// TODO: call to set device for update interval api
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		if( g_pDevIF->get_ver != 0 ) {
			ret = ( *g_pDevIF->get_ver )();
		}
		break;
	case DEVICE_GET_NAME:
		ret = g_accel_name;
		break;
	case SENSOR_SET_DIRECTION: {
		if( g_pDevIF->set_param != 0 ) {
			setting_direction_t param_direction;
			int *p = ( int* )param;
			param_direction.map_x = p[0];
			param_direction.map_y = p[1];
			param_direction.map_z = p[2];
			param_direction.negate_x = p[3];
			param_direction.negate_y = p[4];
			param_direction.negate_z = p[5];
			ret = ( *g_pDevIF->set_param )( &param_direction );
		} else {
			ret = -1;
		}
		break;
	}
	case SENSOR_ACCL_GET_REAL_RAW_DATA: {
		if( g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] != 0 ) {
			( *g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] )( ( void* )param );
		}
		ret = 0;
		break;
	}
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	unsigned int remain = 0;
	if( g_pDevIF->recv != 0 ) {
		remain = ( *g_pDevIF->recv )( g_device.tick );
	}
	if( remain == 0 ) {
		remain = g_device.tick;
		g_device.ts = ts;
		g_device.f_need = 1;
	} else {
		g_device.remain_total = g_device.remain_total + remain;
		if( g_device.remain_total >= g_device.tick ) {
			g_device.remain_total = 0;
			g_device.ts = ts;
			g_device.f_need = 1;
		}
	}
	return ts + remain;
}

struct data {
	frizz_fp   accl[3];
	frizz_fp   gyro[3];
	frizz_fp   magn[1];
} tmp_data;


static int calculate( void )
{
#if 0
	int		result = 0;
	frizz_fp	*fz = ( frizz_fp* )&g_device.data;

	if( g_pDevIF->conv != 0 ) {
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		accel_data.data[0] =  fz[0];
		accel_data.data[1] =  fz[1];
		accel_data.data[2] =  fz[2];
	}
	g_device.f_need = 0;
	return result;
#else
	int		result = 0;
	//ads1191.c　からdata の読み込み
	//	frizz_fp	*fz = ( frizz_fp* )&g_device.data;
	frizz_fp *fz = (frizz_fp*)&tmp_data;


	if( g_pDevIF->conv != 0 ) {
		//result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&tmp_data ); //jump ads1191.c ads1191_conv_accl

		//gpio_set_data( GPIO_NO_3, 0 );

#if 0
		// calibration用セッティング
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		//g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		//g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

		g_device.data[9] =  frizz_div(fz[6], as_frizz_fp( gain_dec)) * FRIZZ_CONST_THOUSAND - as_frizz_fp( 0.0f);
		g_device.data[8] =  g_device.data[9] * as_frizz_fp( gain_dec);
#else
		//offset setting
		g_device.data[0] =  tmp_data.accl[0];
		g_device.data[1] =   tmp_data.accl[1];
		g_device.data[2] =  - tmp_data.accl[2];
		//g_device.data[3] =  as_frizz_fp( 0.0f);
		g_device.data[3] = - tmp_data.gyro[0];
		g_device.data[4] = - tmp_data.gyro[1];
		g_device.data[5] =  tmp_data.gyro[2];

		//g_device.data[9] =  frizz_div(fz[6], as_frizz_fp( gain_dec)) * FRIZZ_CONST_THOUSAND - as_frizz_fp( 0.0f);

		//temp
		//g_device.data[9] =  frizz_div(tmp_data.magn[1], as_frizz_fp( gain_dec)) * FRIZZ_CONST_THOUSAND - as_frizz_fp( 0.0f);
		//g_device.data[8] =  g_device.data[9] * as_frizz_fp( gain_dec);

		g_device.data[6] =  tmp_data.magn[0] * FRIZZ_CONST_THOUSAND - as_frizz_fp( 0.0f);



#endif

	}

	//gpio_set_data( GPIO_NO_3, 0 );

	g_device.f_need = 0;
	return result;
#endif
}

static unsigned int condition( void )
{
	unsigned int	result = 0, res_cond;

	result = get_device_condition( g_device.id );
	if( g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] != 0 ) {
		res_cond = ( *g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] )( 0 );
		if( ( D_RAW_DEVICE_ERR_READ & res_cond ) != 0 ) {
			set_device_condition_phyerr( g_device.id );
		} else {
			reset_device_condition_phyerr( g_device.id );
		}
	}
	return result;
}

sensor_if_t* DEF_INIT( accl_raw )( void )
																																		{
	// ID
	g_device.id = ACCEL_RAW_ID;

	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );

	// init hardware
	if( ( g_pDevIF = basedevice_init( &g_devif[0], NELEMENT( g_devif ), g_device.id ) ) == 0 ) {
		return 0;
	}

	if( g_pDevIF->get_name != 0 ) {
		g_accel_name = ( *g_pDevIF->get_name )();
	}

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.get.condition = condition;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	// param
	g_device.f_active = 0;
	g_device.tick = 1;
	g_device.f_need = 0;
	g_device.ts = 0;
	g_device.remain_total = 0;
	// data
	//g_device.data = as_frizz_fp( 0.0f );

	return &( g_device.pif );
																																		}

