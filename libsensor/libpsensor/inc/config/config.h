/*!******************************************************************************
 * @file    config.h
 * @brief   sensor configuration for frizz board
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __CONFIG_H__
#define __CONFIG_H__


#include "config_base.h"
//#include "config_customer1.h"
//#include "config_customer2.h"
//#include "config_customer3.h"

#endif /* __CONFIG_H__ */


