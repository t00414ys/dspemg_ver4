/*!******************************************************************************
 * @file    std_in.h
 * @brief   virtual standard input sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#if defined(RUN_ON_PC)
#ifndef __STD_IN_H__
#define __STD_IN_H__

#include <stdio.h>
#include "sensor_if.h"
#include "libsensors_id.h"

/**
 * @brief initialize standard input for DEBUG
 *
 * @param fp File pointer
 *
 * @note id: #SENSOR_ID_DEBUG_STD_IN
 */
sensor_if_t* std_in_init( FILE *fp );

#endif
#endif
