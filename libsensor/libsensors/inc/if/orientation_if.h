/*!******************************************************************************
 * @file    orientation_if.h
 * @brief   virtual orientation sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ORIENTATION_IF_H__
#define __ORIENTATION_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup ORIENTATION ORIENTATION
 *  @{
 */
#define ORIENTATION_ID	SENSOR_ID_ORIENTATION //!< Orientation

/**
 * @struct orientation_data_t
 * @brief Output data structure for orientation sensor
 */
typedef struct {
	FLOAT		data[3];	//!< gravity vector data[0]:azimuth[degrees],  data[1]:pitch[degrees], data[2]:roll[degrees]
} orientation_data_t;
/**
 * @name Command List
 * @note none
 */
//@{
//@}
/** @} */
#endif
