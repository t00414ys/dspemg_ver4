/*!******************************************************************************
 * @file    accl_pow_if.h
 * @brief   virtual accel power interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_POW_IF_H__
#define __ACCL_POW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif


/** @defgroup ACCEL_POWER ACCEL POWER
 *  @{
 */
#define ACCEL_POWER_ID	SENSOR_ID_ACCEL_POWER	//!< An accel power interface ID

/**
 * @struct accel_power_data_t
 * @brief Output data structure for accel power sensor
 */
typedef struct {
	FLOAT		data;	//!< accel power[G^2]
} accel_power_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
