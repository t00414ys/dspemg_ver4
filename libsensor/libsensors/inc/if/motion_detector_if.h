/*!******************************************************************************
 * @file    motion_detector_if.h
 * @brief   virtual motion detector sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MOTION_DETECTOR_IF_H__
#define __MOTION_DETECTOR_IF_H__

#include "libsensors_id.h"

/** @defgroup MOTION_DETECTOR MOTION DETECTOR
 *  @{
 */
#define MOTION_DETECTOR_ID SENSOR_ID_MOTION_DETECTOR	//!< An motion detector interface ID

/**	@struct motion_data_t
 *	@brief Output data structure for motion detector sensor
 */
typedef struct {
	unsigned int activity;			//!< motion status 0x02:REST 0x03:WALK 0x04:RUN
	unsigned int step_cnt;			//!< step count
	unsigned int step_freq;			//!< step frequency (unit: step/second)
	unsigned int step_freq_m;		//!< step frequency (unit: step/minute)
} motion_data_t;

/**
 * @name Command List
 */
//@{

/**
 * @brief	set period of stop state detected (unit: minute)
 * @param	cmd_code	MOTION_DET_CMD_SET_STOP_PERIOD
 * @return	0:OK other:NG
 */
#define MOTION_DET_CMD_SET_STOP_PERIOD				0x00

//@}

/** @} */
#endif
