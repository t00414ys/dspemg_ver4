/*!******************************************************************************
 * @file    pres_raw_if.h
 * @brief   virtual pressure raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup PRESSURE_RAW PRESSURE RAW
 *  @ingroup physicalgroup Physical Sensor
 *  @brief pressure raw sensor<br>
 *  @brief -# <B>Contents</B><br>
 *  @brief A sensor for taking out a pressure component.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_PRESSURE_RAW <br>
 *  @brief Parent Sensor ID : none<br>
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Detection Timing : Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __PRES_RAW_IF_H__
#define __PRES_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup PRESSURE_RAW PRESSURE RAW
 *  @{
 */

#define PRESSURE_RAW_ID	SENSOR_ID_PRESSURE_RAW //!< Pressure from Physical Sensor

/**
 * @struct pressure_raw_data_t
 * @brief Output data structure for a pressure raw sensor
 */
typedef struct {
	FLOAT		data;	//!< pressure raw data[hPa]
} pressure_raw_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}
/** @} */
#endif
