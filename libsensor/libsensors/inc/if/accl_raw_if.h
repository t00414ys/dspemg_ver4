/*!******************************************************************************
 * @file    accl_raw_if.h
 * @brief   virtual accel raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup ACCEL_RAW ACCEL RAW
 *  @ingroup physicalgroup Physical Sensor
 *  @brief acceleration raw sensor<BR>
 *  @brief -# <B>Contents</B><BR>
 *  @brief A sensor for taking out an accelerometer component.<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_ACCEL_RAW <BR>
 *  Parent Sensor ID : none<BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief	Detection Timing : Continuous (This sensor type output sensor
 *			data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __ACCL_RAW_IF_H__
#define __ACCL_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup ACCEL_RAW ACCEL RAW
 *  @{
 */

#define ACCEL_RAW_ID	SENSOR_ID_ACCEL_RAW //!< Acceleration from Physical Sensor interface ID

/**
 * @struct accel_raw_data_t
 * @brief Output data structure for an accel sensor
 */
typedef struct {
	//FLOAT		data[3];	//!< data[0]:X-axis Acceleration[G], data[1]:Y-axis Acceleration[G], data[2]:Z-axis Acceleration[G]
	FLOAT		data[7];	//!< data[0]:X-axis Acceleration[G], data[1]:Y-axis Acceleration[G], data[2]:Z-axis Acceleration[G]
} accel_raw_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
#define SENSOR_ACCL_GET_REAL_RAW_DATA	0x01
//@}

/** @} */
#endif
