/*!******************************************************************************
 * @file    accl_pedo_if.h
 * @brief   virtual accel pedometer sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_PEDO_IF_H__
#define __ACCL_PEDO_IF_H__

#include "libsensors_id.h"

/** @defgroup ACCEL_PEDOMETER ACCEL PEDOMETER
 *  @{
 */
#define ACCEL_PEDOMETER_ID SENSOR_ID_ACCEL_PEDOMETER	//!< An accel pedometer interface ID

/**	@struct accel_pedometer_data_t
 *	@brief Output data structure for accel pedometer sensor
 */
typedef struct {
	unsigned int step_cnt;			//!< step count
	unsigned int time_interval;		//!< time interval (unit: msecond)
} accel_pedometer_data_t;

/**
 * @name Command List
 */
//@{

/**
 * @brief	Clear counter
 * @param	cmd_code	ACCEL_PEDOMETER_CMD_CLEAR_COUNT
 * @return	0:OK other:NG
 */
#define ACCEL_PEDOMETER_CMD_CLEAR_COUNT		0x00

/**
 * @brief	Set threshold for output step counter
 * @param	cmd_code	ACCEL_PEDOMETER_CMD_SET_OUTPUT_THRESHOLD
 * @param param[0] unsigned int  output threshold
 * @return	0:OK other:NG
 */
#define ACCEL_PEDOMETER_CMD_SET_OUTPUT_THRESHOLD		0x01

//@}

/** @} */
#endif
