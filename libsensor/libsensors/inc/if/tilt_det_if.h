/*!******************************************************************************
 * @file    tilt_det_if.h
 * @brief   virtual tilt detector if sensor interface
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __TILT_DET_IF_H__
#define __TILT_DET_IF_H__

#include "libsensors_id.h"

/** @defgroup TILT_DET TILT DETECTOR
 *  @{
 */
#define TILT_DET_ID SENSOR_ID_TILT_DETECTOR	//!< An tilt detector sensor interface ID

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
