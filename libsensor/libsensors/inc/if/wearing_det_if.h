/*!******************************************************************************
 * @file    wearing_det_if.h
 * @brief   virtual wearing detector sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __WEARING_DET_IF_H__
#define __WEARING_DET_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup SENSOR_ID_WEARING_DETECTOR WEARING DETECTOR
 *  @{
 */

#define WEARING_DETECTOR_ID	SENSOR_ID_WEARING_DETECTOR	//!< wearing detector sensor interface ID

/**
 * @struct wearing_det_data_t
 * @brief Output data structure for wearing detecto sensor
 */
typedef struct {
	unsigned int	data;		//!< 0x00000000 not wearing , 0x00000001  wearing
} wearing_det_data_t;

/**
 * @name Command List
 */
//@{

//@}

/** @} */
#endif
