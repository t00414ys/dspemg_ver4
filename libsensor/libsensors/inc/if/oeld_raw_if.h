/*!******************************************************************************
 * @file    oeld_raw_if.h
 * @brief   virtual oeld raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup OELD_RAW OELD RAW
 * @ingroup physicalgroup Physical Sensor
 *  @brief OELD sensor
 *  @brief -# <B>Contents</B><BR>
 *  @brief Debug use only<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_OELD_RAW <BR>
 *  @brief Parent Sensor ID : none <BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief Detection Timing : Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __OELD_RAW_IF_H__
#define __OELD_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup OELD_RAW OELD RAW
 *  @{
 */

#define OELD_RAW_ID	SENSOR_ID_OELD_RAW	//!< oeld raw sensor interface ID


/**
 * @name Command List
 */
//@{

/**
 * @brief Fill Rect OELD
 * @brief Refer to #ST_FILL_RECT for parameter
 * @param param[0] int: left
 * @param param[1] int: top
 * @param param[2] int: width
 * @param param[3] int: height
 * @param param[3] unsigned char: fill char
 * @return 0: void
 */
#define OELD_FILL_RECT				(0x00)

/*! @brief		picture print to oled
 * @brief Refer to #ST_DRAW_PICTURE for parameter
 * @param[in]	rate	rate of magnification
 * @param[in]	p_x		drawing start point x(dot)
 * 	@arg	0 to 127
 * @param[in]	p_y		drawing start point y(line)
 * 	@arg	0 to 7 (1line = 8dots)
 * @param[in]	p_buf	picture data
 * @return		void
*/
#define OELD_DRAW_PICTURE			(0x01)

/*! @brief		number print to oled
 * @brief Refer to #ST_NUM_PRINT for parameter
 * @param[in]	rate	rate of magnification
 * @param[in]	p_x		drawing start point x(dot)
 * 	@arg	0 to 127
 * @param[in]	p_y		drawing start point y(line)
 * 	@arg	0 to 7 (1line = 8dots)
 * @param[in]	digits	display digit
 * @param[in]	input_num	input number
 * @return		void
 * @attention	aligning to the right. when if input number exceed the digits, print '-'
*/
#define OELD_NUM_PRINT				(0x03)

/*! @brief		characters print to oled
 * @brief Refer to #ST_CHAR_PRINT for parameter
 * @param[in]	rate	rate of magnification
 * 	@param[in]	p_x		drawing start point x(dot)
 * 	@arg	0 to 127
 * @param[in]	p_y		drawing start point y(line)
 * 	@arg	0 to 7 (1line = 8dots)
 * @param[in]	characters	input characters
 * 	@arg	character code 0x20 to 0x
 * @return		void
 * @attention	please terminate characters a '\0' character.
 * @attention	invalid character code convert to '?' character.
*/
#define OELD_CHAR_PRINT				(0x04)

//@}

/** @} */

/**
 * @struct ST_FILL_RECT
 * @brief structure for OELD_FILL_RECT
 */
typedef struct {
	unsigned char	left;	//!< left
	unsigned char	top;	//!< top
	unsigned char	width;	//!< width
	unsigned char	height;	//!< height
	unsigned char	data;	//!< data
} ST_FILL_RECT;

/**
 * @struct ST_DRAW_PICTURE
 * @brief structure for ST_DRAW_PICTURE
 */
typedef struct {
	unsigned char	rate;	//!< rate of magnification
	unsigned char	p_x;	//!< drawing start point x(dot)
	unsigned char	p_y;	//!< drawing start point y(line)
	unsigned char	p_w;	//!< picture width
	unsigned char	p_l;	//!< picture height
	unsigned char	*p_buf;	//!< picture data
} ST_DRAW_PICTURE;

/**
 * @struct ST_NUM_PRINT
 * @brief structure for ST_NUM_PRINT
 */
typedef struct {
	unsigned char	rate;	//!< rate of magnification
	unsigned char	p_x;	//!< drawing start point x(dot)
	unsigned char	p_y;	//!< drawing start point y(line)
	unsigned char	digits;	//!< display digit
	unsigned int	input_num;	//!< input number
} ST_NUM_PRINT;

/**
 * @struct ST_CHAR_PRINT
 * @brief structure for ST_CHAR_PRINT
 */
typedef struct {
	unsigned char	rate;	//!< rate of magnification
	unsigned char	p_x;	//!< drawing start point x(dot)
	unsigned char	p_y;	//!< drawing start point y(line)
	char			*characters;	//!< input characters
} ST_CHAR_PRINT;

extern const unsigned char battery[];	//!< width : length = 7 : 3
extern const unsigned char walk_man[];	//!< width : length = 32 : 2
extern const unsigned char footprint[];	//!< width : length = 32 : 2
extern const unsigned char tape[];		//!< width : length = 32 : 2
extern const unsigned char m_char[];	//!< width : length = 16 : 1
extern const unsigned char s_char[];	//!< width : length = 16 : 1
extern const unsigned char t_char[];	//!< width : length = 16 : 1
extern const unsigned char slash[];		//!< width : length = 16 : 1
extern const unsigned char bpm_char [];	//!< width : length = 14 : 1
extern const unsigned char hartlate[];	//!< width : length = 11 : 1
extern const unsigned char hyphen[];	//!< width : length = 12 : 3
extern const unsigned char haert[2][12];	//!< width : length = 12 : 3
extern const unsigned char number_dx[11][36];	//!< width : length = 12 : 3
//extern const unsigned char number_dx_big[11][120];	//!< width : length = 24 : 5
extern const unsigned char Ascii_1[97][5];	//!< width : length = 5 : 1
extern const unsigned char gamen[];			//!< not use
extern const unsigned char font_x2_LUT[16];		//!< twice of size
extern const unsigned short font_x3_LUT[16];	//!< 3times of size

#endif
