/*!******************************************************************************
 * @file    gravity_if.h
 * @brief   virtual gravity sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GRAVITY_IF_H__
#define __GRAVITY_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif


/** @defgroup GRAVITY GRAVITY
 *  @{
 */
#define GRAVITY_ID	SENSOR_ID_GRAVITY	//!< A gravity sensor interface ID

/**
 * @struct gravity_data_t
 * @brief Output data structure for gravity direction sensor
 */
typedef struct {
	FLOAT		data[3];	//!< gravity vector [G] data[0]:X-axis Component[G], data[1]:Y-axis Component[G], data[2]:Z-axis Component[G]
} gravity_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
