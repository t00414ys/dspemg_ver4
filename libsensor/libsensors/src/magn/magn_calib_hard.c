/*!******************************************************************************
 * @file    magn_calib_hard.c
 * @brief   virtual magnet calibration hard sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "mag_util.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "libsensors_id.h"
#include "magn_calib_hard.h"
#include "if/magn_param_if.h"
#include "if/magn_calib_hard_if.h"

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[2];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_soft;
	sensor_if_get_t		*p_param;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	// data
	frizz_fp4w			data;
	// parameter
	frizz_fp4w_t		ofst;
} device_sensor_t;

static device_sensor_t g_device;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 3;
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == SENSOR_ID_MAGNET_CALIB_SOFT ) {
		g_device.p_soft = gettor;
	}
	if( gettor->id() == SENSOR_ID_MAGNET_PARAMETER ) {
		g_device.p_param = gettor;
	}
}

static void notify_updated( unsigned char sen_id, SENSOR_NOTIFY ntfy );
static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_soft->id(), g_device.tick );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_soft->id(), g_device.f_active );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_param->id(), g_device.f_active );
		if( f_active ) {
			notify_updated( SENSOR_ID_MAGNET_PARAMETER, SENSOR_NOTIFY_UPDATED_DATA );
		}
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	if( g_device.tick != tick ) {
		g_device.tick = tick;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_soft->id(), g_device.tick );
	}
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}


static unsigned int notify_ts( unsigned int ts )
{
	g_device.ts = ts;
	g_device.f_need = 1;
	return ts + g_device.tick;
}

static void notify_updated( unsigned char sen_id, SENSOR_NOTIFY ntfy )
{
	if( sen_id == SENSOR_ID_MAGNET_PARAMETER ) {
		magnet_parameter_data_t *p;
		g_device.p_param->data( ( void** )&p, &g_device.ts );
		if( p->f_calib ) {
			g_device.ofst.z[0] = p->param[9];
			g_device.ofst.z[1] = p->param[10];
			g_device.ofst.z[2] = p->param[11];
		}
	}
}

static int calculate( void )
{
	frizz_fp4w *p;
	g_device.p_soft->data( ( void** )&p, &g_device.ts );
	g_device.data = *p - g_device.ofst.w;
	g_device.f_need = 0;
	return 1;
}

sensor_if_t* DEF_INIT( magn_calib_hard )( void )
{
	// ID
	g_device.id = MAGNET_CALIB_HARD_ID;
	g_device.par_ls[0] = SENSOR_ID_MAGNET_CALIB_SOFT;
	g_device.par_ls[1] = SENSOR_ID_MAGNET_PARAMETER;
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = notify_updated;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	g_device.tick = 100;
	g_device.f_need = 0;
	g_device.ts = 0;
	// data
	g_device.data *= as_frizz_fp( 0.0f );
	// parametr
	g_device.ofst.w *= as_frizz_fp( 0.0f );

	return &( g_device.pif );
}

